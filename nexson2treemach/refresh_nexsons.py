#!/usr/bin/env python
if __name__ == '__main__':
    from nexson2treemach import get_processing_paths_from_prefix, get_default_dir_dict
    from nexson2treemach import get_list_of_dirty_nexsons, download_nexson_from_phylografter, LockPolicy
    import sys
    import os
    lock_policy = LockPolicy()
    dd = get_default_dir_dict()
    if len(sys.argv) == 1:
        to_download, download_db = get_list_of_dirty_nexsons(dd)
        if not to_download:
            sys.stderr.write('No studies have been modified since the last refresh.\n')
            sys.exit(0)
    else:
        to_download, download_db = sys.argv[1:], None
    while len(to_download) > 0:
        n = to_download.pop(0)
        study = str(n)
        paths = get_processing_paths_from_prefix(study, **dd)
        if not download_nexson_from_phylografter(paths, download_db, lock_policy):
            sys.exit('NexSON "%s" could not be refreshed\n' % paths['nexson_path'])


