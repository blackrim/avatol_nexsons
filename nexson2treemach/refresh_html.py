#!/usr/bin/env python
if __name__ == '__main__':
    from nexson2treemach import get_processing_paths_from_prefix, get_default_dir_dict, target_is_dirty, get_study_filename_list
    from nexson2treemach import htmlize_treemachine_output, LockPolicy
    import sys
    study_list = sys.argv[1:]
    lock_policy = LockPolicy()
    dd = get_default_dir_dict()
    if not study_list:
        study_list = get_study_filename_list(dd)
    ran_some = False
    for pref in study_list:
        paths = get_processing_paths_from_prefix(pref, **dd)
        n_path = paths['nexson']
        e_path = paths['treemachine_log']
        dest_path = paths['html']
        err_path = paths['html_err']
        needs_updating = target_is_dirty([n_path, e_path], [dest_path, err_path])
        if needs_updating:
            ran_some = True
            if not htmlize_treemachine_output(paths, lock_policy):
                sys.exit('status html "%s" could not be created\n' % dest_path)
    if not ran_some:
        sys.stderr.write('Up to date.\n')
