#!/usr/bin/env python



if __name__ == '__main__':
    import sys, os
    import codecs
    import locale
    sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
    import json
    from nexson2treemach import get_processing_paths_from_prefix, get_default_dir_dict
    from nexson2treemach.parse_nexson import Study
    from nexson2treemach import process_treemachine_log_info
    from nexson2treemach import write_status_obj_as_html
    try:
        study_id = sys.argv[1]
    except:
        sys.exit('Expecting a study numver as the only arg\n ')
    dd = get_default_dir_dict()
    default_study_id = study_id
    
    paths = get_processing_paths_from_prefix(study_id, **dd)
    
    oinp = open(paths['nexson'], 'rU')
    inp = open(paths['treemachine_log'], 'rU')

    log_object = json.load(inp)
    orig_object = Study(json.load(oinp))
    
    status_obj = process_treemachine_log_info(log_object, orig_object, study_id)
    
    status_stream = open(paths['status_json'], 'w')
    json.dump(status_obj, status_stream, sort_keys=True, indent=1)
    status_stream.close()

    output = sys.stdout
    write_status_obj_as_html(status_obj, output)