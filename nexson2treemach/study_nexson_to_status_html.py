#!/usr/bin/env python
import sys, os

OVERRIDE_CONTINUE_ON_ERROR = False
def sigterm_handler(signum, frame):
    global OVERRIDE_CONTINUE_ON_ERROR
    OVERRIDE_CONTINUE_ON_ERROR = True
    raise RuntimeError("SIGTERM received")

def print_help():
    n = sys.argv[0]
    sys.stderr.write('''
Usage:
   %s [options] treemachine_location [study id]
where treemachine_location should be either
    1. the filepath for the treemachine db, or
    2. the domain and root prefix for treemachine web services

Such as:

    $ python %s -a /home/ott.db 9

to try study 9 through a local installation of tree machine or:

    $ python %s -a http://dev.opentreeoflife.org/treemachine 9

to use the webservices

options:
    -d/path/to/avatol_nexsons/dir
    -h  print this help message and exit
    -l  refresh list of altered NexSON files from phylografter
    -n  refresh NexSON from phylografter
    -t  refresh JSON produced by running treemachine
    -s  refresh the richer status JSON produced from the treemachine JSON
    -w  refresh the html from the richer JSON
    -a  try all refresh operations that appear necessary
    -f  force refresh (even in result timestamp is newer)
    -x  continue even if one study file fails
''' % (n, n, n))

if __name__ == '__main__':
    import json
    from nexson2treemach import open_for_group_write
    from nexson2treemach import get_processing_paths_from_prefix, get_default_dir_dict, target_is_dirty, get_study_filename_list
    from nexson2treemach import get_previous_list_of_dirty_nexsons
    from nexson2treemach import get_list_of_dirty_nexsons, download_nexson_from_phylografter, LockPolicy
    from nexson2treemach import run_treemachine_pg_import_check
    from nexson2treemach import refresh_of_status_json_from_treemachine_path, refresh_html_from_status_obj
    import os
    import signal
    options = {}
    arguments = []
    top_level = os.curdir
    for arg in sys.argv[1:]:
        if arg.startswith('-'):
            flag = arg[1]
            if len(arg) > 2:
                if flag == 'd':
                    top_level = arg[2:]
                else:
                    options[flag] = arg[2:]
            else:
                options[flag] = True
        else:
            arguments.append(arg)
    if 'h' in options:
        print_help()
        sys.exit(-1)
    do_refresh_nexson = ('n' in options) or ('a' in options)
    do_refresh_nexson_list = ('l' in options) or ('a' in options)
    do_refresh_treemachine_log = ('t' in options) or ('a' in options)
    do_refresh_status_json = ('s' in options) or ('a' in options)
    do_refresh_html = ('w' in options) or ('a' in options)
    continue_on_failure = 'x' in options
    force_refresh_of_newer = 'f' in options
    if not (do_refresh_nexson or do_refresh_treemachine_log or do_refresh_status_json or do_refresh_html):
        sys.exit('No refresh actions were requested!')
    try:
        treemachine_contact_point = arguments[0]
        if not treemachine_contact_point.startswith('http://'):
            assert os.path.exists(treemachine_contact_point)
            assert os.path.isdir(treemachine_contact_point)
    except:
        sys.exit('Expecting the first arg to the be the path to your treemachine db')
    lock_policy = LockPolicy()
    dd = get_default_dir_dict(top_level)
    if do_refresh_nexson or do_refresh_nexson_list:
        if do_refresh_nexson_list:
            dirty_nexsons, download_db = get_list_of_dirty_nexsons(dd)
        else:
            dirty_nexsons, download_db = get_previous_list_of_dirty_nexsons(dd)
        dirty_nexsons = [str(i) for i in dirty_nexsons]
    else:
        download_db = None
    ran_some = False
    if len(arguments) > 1:
        study_list = arguments[1:]
        processing_all = False
    else:
        study_list = get_study_filename_list(dd)
        processing_all = True
    some_failures = False
    signal.signal(signal.SIGTERM, sigterm_handler)
    for study in study_list:
        if processing_all:
            sys.stderr.write('Processing ' + study + '\n')
        #################
        # grab the paths
        #################
        paths = get_processing_paths_from_prefix(study, **dd)
        nexson_path = paths['nexson']
        lockfile = nexson_path + '.studylock'
        study_lock_policy = LockPolicy()
        study_was_locked, owns_study_lock = study_lock_policy.wait_for_lock(lockfile)
        try:
            if not owns_study_lock:
                raise RuntimeError('Could not obtain lock for this study. Lockfile "%s" is present.' % lockfile)
            if do_refresh_status_json:
                pid_file = open_for_group_write(paths['launched_study_proc'], 'w')
                pid_file.write('%s\n' % str(os.getpid()))
                pid_file.close()
            treemachine_log_path = paths['treemachine_log']
            treemachine_err_path = paths['treemachine_err']
            html_path = paths['html']
            html_err_path = paths['html_err']
            status_json_path = paths['status_json']
            #################
            # get NexSON
            #################
            if do_refresh_nexson and (force_refresh_of_newer or (study in dirty_nexsons)):
                try:
                    dirty_nexsons.remove(study)
                except:
                    pass
                if not download_nexson_from_phylografter(paths, download_db, lock_policy):
                    raise RuntimeError('NexSON "%s" could not be refreshed\n' % nexson_path)
            #####################
            # get treemachine log
            #####################
            if do_refresh_treemachine_log:
                needs_updating = force_refresh_of_newer or target_is_dirty([nexson_path], [treemachine_log_path])
                if needs_updating:
                    ran_some = True
                    if treemachine_contact_point.startswith('http://'):
                        r = run_treemachine_pg_import_check(paths, lock_policy, treemachine_domain=treemachine_contact_point)
                    else:
                        r = run_treemachine_pg_import_check(paths, lock_policy, treemachine_db=treemachine_contact_point)
                    if not r:
                        raise RuntimeError('treemachine log "%s" could not be created\n' % treemachine_log_path)
            #####################
            # parse treemachine log to status JSON
            #####################
            status_obj = None
            if do_refresh_status_json:
                needs_updating = force_refresh_of_newer or target_is_dirty([nexson_path, treemachine_log_path], [status_json_path])
                if needs_updating:
                    ran_some = True
                    status_obj = refresh_of_status_json_from_treemachine_path(paths, lock_policy)
                    if status_obj is None:
                        raise RuntimeError('status JSON "%s" could not be created\n' % status_json_path)
            #####################
            # convert status JSON to html
            #####################
            if do_refresh_html:
                needs_updating = force_refresh_of_newer or target_is_dirty([status_json_path], [html_path])
                if needs_updating:
                    if status_obj is None:
                        status_obj = json.load(open(status_json_path, 'rU'))
                    ran_some = True
                    if not refresh_html_from_status_obj(paths, status_obj, lock_policy):
                        raise RuntimeError('status html "%s" could not be created\n' % html_path)
        except:
            study_lock_policy.remove_lock()
            import traceback
            traceback.print_exc(file=sys.stderr)
            traceback.print_stack(file=sys.stderr)
            if continue_on_failure and (not OVERRIDE_CONTINUE_ON_ERROR):
                some_failures = True
            else:
                sys.exit(-1)

        else:
            study_lock_policy.remove_lock()
    if not ran_some:
        sys.stderr.write('Up to date.\n')
    if some_failures:
        sys.exit(-1)
    sys.exit(0)
