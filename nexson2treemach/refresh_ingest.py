#!/usr/bin/env python
if __name__ == '__main__':
    import sys, os
    from nexson2treemach import LockPolicy
    from nexson2treemach import target_is_dirty, get_processing_paths_from_prefix, get_default_dir_dict, run_treemachine_pg_import_check
    try:
        treemachine_contact_point = sys.argv[1]
        if not treemachine_contact_point.startswith('http://'):
            assert os.path.exists(treemachine_contact_point)
            assert os.path.isdir(treemachine_contact_point)
    except:
        sys.exit('Expecting the first arg to the be the path to your treemachine db')
    dd = get_default_dir_dict()
    if len(sys.argv) == 3:
        study_list = [sys.argv[2]]
    else:
        study_list = get_study_filename_list(dd)
    ran_some = False
    lock_policy = LockPolicy()
    for pref in study_list:
        paths = get_processing_paths_from_prefix(pref, **dd)
        n_path = paths['nexson']
        l_path = paths['treemachine_log']
        err_path = paths['treemachine_err']
        needs_updating = target_is_dirty([n_path], [l_path])
        if needs_updating:
            ran_some = True
            if treemachine_contact_point.startswith('http://'):
                run_treemachine_pg_import_check(paths, lock_policy, treemachine_domain=treemachine_contact_point)
            else:
                run_treemachine_pg_import_check(paths, lock_policy, treemachine_db=treemachine_contact_point)
    if not ran_some:
        sys.stderr.write('Up to date.\n')
