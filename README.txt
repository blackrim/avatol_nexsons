Snapshots of NexSON files from phylografter. Numerical filenames are JSON serializations of 
the studies in phylografter.

Grabbing the NexSON files
=========================

getNexsons.sh is the old way. It will always grab all studies (by requesting a very early modification time).

The nexson2treemach directory is a crude python module for synchronizing the NexSON files, running them through treemachine's pgloadind command and parsing the output of treemachine into simple html.

Temporary initial setup
=======================

Much of the code has moved to the opentree web app repo. I've had some
issues with importing modules in web2py, so (for the time being) there
is an obnoxious system of symlink required to make the scripts in this
repo work correctly.  To set this up:

    $ ..
    $ git clone git@github.com:OpenTreeOfLife/opentree.git
    $ cd avatol_nexsons/nexson2treemach
    $ ln -s ../../opentree/webapp/modules/parse_nexson.py .
    $ ln -s ../../opentree/webapp/modules/nexson2treemachine.py __init__.py

Usage
=====

To use nexson2treemach, you'll have to add this top level directory to your PYTHONPATH (or add a symlink from a dir on your PYTHONPATH 
tonexson2treemach)

nexson2treemach/refresh_nexsons.py tries to only grab the studies that have changed. It uses a simple flatfile db ".to_download.json" to do this. The format of that file is just the JSON returned by phylografter's ...study/modified_list.json/url... web service.

Do not edit .to_download.json.

Do git-commit your updaded .to_download.json file when you git-commit the changed NexSON files.

To update, simply run:

    $ PYTHONPATH=$PWD:$PYTHONPATH python nexson2treemach/refresh_nexsons.py

This should:
  1. update the list of JSON files to be downloaded ( .to_download.json ),
  2. start downloading them one at at time,
  3. update the file on your filesystem if the JSON content has changed, and
  4. refresh .to_download.json after each file that has been downloaded (so that you can resume where you left off should you need to kill the script.

Alternatively, run:
    $ ./refresh

This will:
  1. pull any updated studies from the git repo
  2. pull updated studies from phylografter (as above)
  3. commit and push this back to the git repo

If you suspect that you ever need to refresh all of the studies (e.g. if the phylografter code base changes the way that it emits NexSON), simply delete .to_download.json (note that doing this induces a heavy load on phylografter, so be judicious with this).


NexSON to html status page
==========================

This is a *really* crude way of generating html status pages that try to describe whether or not a study in phylografter will be able to get one of its trees into treemachine.

Installation
------------

 1. usage requires the symlink and PYTHONPATH stuff mentioned above

 2. usage requires that you have an executable called "treemachine" on your PATH (see https://raw.github.com/mtholder/ot-vagrant/master/opentree-tools/bin/treemachine for an example of a shell script)

 3. you must have run treemachine's inittax command on the current OTT taxonomy, and have that filepath handy to use as a command line argument.

Usage
-----

    $ python nexson2treemach/study_nexson_to_status_html.py <path-to-treemachine-db> 9

will refresh the NexSON for study 9 (if it has changed), give that to treemachine's pgloadind command, and write an html status page.

Omitting the study number (9, in the example above) should update all studies.

Side effects
------------
A subdirectory called "ingest" will be used as scratch space for the stdout and stderr of treemachine and the stderr of the html-producing step.

A subdirectory called "status" will store the html output.

The contents of both of those directories will be overwritten by the script.

