# reads nexsons into couchdb
# assumes 1. all files without extensions are nexsons
# 2. all files are in current working dir
# neither of these assumptions should be required
# it also doesn't do any checking about duplicates or 
# modifications to files
import sys
import os
import couchdb
import json

def usage() :
    print "Usage: import2couchDB url_to_counchdb database_name"

# crappy command-line parameter parsing
couchdb_url=sys.argv[1]
database_name=sys.argv[2]
print "couchDB at",couchdb_url
database_url=couchdb_url+database_name
print "database at",database_url

# open connection to server and define database
couch = couchdb.Server()
db = couch[database_name]

# get the list of files to import
# this is everything where the filename is all digits
filelist=os.listdir(os.getcwd())
jsons=[]
for file in filelist:
    if not file.isdigit():
        print "skipping file ",file
    else:
        jsons.append(file)

nfiles = len(jsons)
print "putting",nfiles,"documents into couchDB"

# get n UUIDs, where n=number of json documents
# put them in a list
uuids = couch.uuids(nfiles)

# add {_id:uuid} to contents of each file
# and load into couchdb
# todo: should check to see if file exists / is modified
for doc in jsons:
    data = json.load(open(doc))
    uuid = uuids.pop()
    data['_id'] = uuid
    docid,docrev=db.save(data)
    print docid,docrev





    
