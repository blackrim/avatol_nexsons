#!/usr/bin/env python
import json
import sys
try:
    indent = int(sys.argv[1])
except:
    indent = 0
json.dump(json.load(sys.stdin), sys.stdout, sort_keys=True, indent=indent)
