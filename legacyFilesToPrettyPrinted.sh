#!/bin/bash
set -x

for (( j=1; j<3000; j++ ))
do
	if test -f $j
	then
		cat $j | ./pretty-print.py > "pp${j}.txt"
		mv "pp${j}.txt" $j
	fi
done

